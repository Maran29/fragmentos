package parametros.marco.vega.lucas.pm.facci.fragmentos;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener, FrgUn.OnFragmentInteractionListener,FrgDs.OnFragmentInteractionListener {


    //Practica #5

    Button botonFragUno, botonFragDos;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        switch (item.getItemId()){
            case R.id.OpcionLogin:

                Dialog dialogoLogin= new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button botonAutenticar=(Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText CajaUsuario=(EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText CajaClave= (EditText)dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(ActividadPrincipal.this, CajaUsuario.getText().toString()+""+
                                CajaClave.getText().toString(),Toast.LENGTH_SHORT).show();
                    }
                });
                dialogoLogin.show();

                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }



    //practica #4



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        botonFragUno=(Button)findViewById(R.id.btnFrgUno);
        botonFragDos=(Button)findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnFrgUno:
                FrgUn fragmentoUno =new FrgUn();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDs fragmentoDos =new FrgDs();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
